package twactors

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Stash}
import akka.event.LoggingReceive
import twactors.PC._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Random

object PC {

  case object Init

  case class Put(x: Long)

  case object Get

  case object ProduceDone

  case class ConsumeDone(x: Long)

}

class Producer(name: String, buf: ActorRef) extends Actor {

  def receive: Receive = waitingForInit

  private def waitingForInit: Receive = {
    case Init =>
      println(s"$name: initialized")
      context.become(initialized)
  }

  private def initialized: Receive = {
    produce()

    {
      case ProduceDone => produce()
    }
  }

  private def produce(): Unit = {
    val x = Random.nextInt()
    println(s"$name: producing $x")
    buf ! Put(x)
  }

}

class Consumer(name: String, buf: ActorRef) extends Actor {

  def receive: Receive = waitingForInit

  private def waitingForInit: Receive = {
    case Init =>
      println(s"$name: initialized")
      context.become(initialized)
  }

  private def initialized: Receive = {
    buf ! Get

    {
      case ConsumeDone(x) =>
        println(s"$name: consumed $x")
        buf ! Get
    }
  }

}

class Buffer(n: Int) extends Actor with Stash {

  import PC._

  private val buf = new Array[Long](n)
  private var count = 0

  private var waitingForProducer = false
  private var waitingForConsumer = false

  def receive: Receive = LoggingReceive {
    case Put(x) if count < n =>
      buf.update(count, x)
      count += 1
      sender ! ProduceDone

      if (waitingForProducer) {
        waitingForProducer = false
        unstashAll()
      }

    case Get if count > 0 =>
      count -= 1
      sender ! ConsumeDone(buf(count))

      if (waitingForConsumer) {
        waitingForConsumer = false
        unstashAll()
      }

    case Get =>
      waitingForProducer = true
      stash()

    case Put(_) =>
      waitingForConsumer = true
      stash()
  }
}

object ProdConsMain extends App {

  import PC._

  val system = ActorSystem("ProdCons")

  val bufferCapacity = 25
  val producerCount = 5
  val consumerCount = 5

  val buffer = system.actorOf(Props(new Buffer(bufferCapacity)))

  for (i <- 0 to producerCount) {
    system.actorOf(Props(new Producer(s"producer-${i + 1}", buffer))) ! Init
  }

  for (i <- 0 to consumerCount) {
    system.actorOf(Props(new Consumer(s"consumer-${i + 1}", buffer))) ! Init
  }

  Await.result(system.whenTerminated, Duration.Inf)
}
